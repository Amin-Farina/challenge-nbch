# Challenge Nuevo Banco Del Chaco  

### Enunciado del ejercicio:

##### Datos del Archivo

- Dado el archivo artist_credit.csv
- Para descargar el archivo (aquí)[https://drive.google.com/drive/folders/1h2Aqxn6WZ7C9LNvEZjMLSXzFebTiJhzf?usp=sharing]
- Tiene los siguientes campos
    - Id = identificador de registro
    - name = Nombre del Artista
    - artist_count = Créditos
    - ref_count = Referecias
    - created = Fecha de Creación


Se requiere generar un programa en Kettle, que sea capaz de procesar el archivo  artist_credit.csv, el programa debe recibir una fecha por parámetro al ejecutarlo.

Se deberán generar tres salidas.

1. Generar una tabla de salida en una base de datos (Cualquier DB), en la tabla se deberán guardar todos los artistas cuya fecha de creación (created) sea mayor a la fecha ingresada por parámetro.
Campos de la tabla de salida
    - ID
    - name
    - created
(La tabla debe ser cargada con los datos ordenados por campo name en forma ascendente.)


2. Generar una salida en un archivo Excel, en el que se guardarán todos los artistas cuya fecha de creación (created) sea menor a la fecha ingresada por parámetro.
Campos del archivo de salida
- name
- created
- suma = artist_count + ref_count (Colocar la suma de ambos valores)
(El archivo debe ser cargado ordenado por id de forma descendente.)

3. Generar una salida en un archivo tipo .csv en la que se deberán guardar todos los artistas donde el año de la fecha de creación sea igual al año de la fecha ingresada por parámetro.
Campos del archivo de salida
- name
- created
- periodo
- concat = name + periodo (Colocar la concatenación de ambos campos)
(El archivo debe ser cargado ordenado por fecha (created) de forma descendiente.)


4. Subir el código de la/s transformaciones/Jobs a un repositorio git público (archivos .ktr y .kjb)

5. Generar un archivo .bat o .sh que sea capas de ejecutar la aplicación kettle utilizando pan o kitchen, subir al repositorio él archivo.